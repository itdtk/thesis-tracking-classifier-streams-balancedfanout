# Setup Instructions

Adapt /src/main/resources/application.conf accordingly.

# Parallelization of Vehicle Detection in Streams

Although, applying a SVM classifier in order to detect vehicles does not require to process the video frames in order, a stream-based design can still be beneficial.

![Design-Tracking-Classifier-Stream.png](https://bitbucket.org/repo/75r84p/images/152697496-Design-Tracking-Classifier-Stream.png)

This figure visualises the flow of frames from their `VideoSource` stage to a `Sink` stage that persists the detected vehicles' positions in some storage. Since OpenCV's `multi-scale(...)` method requires a frame in greyscale, each frame has to be converted via the `GreyscaleConversion` stage. As with the frame-pulling pattern, the number of `Detector` stages depends on the system environment and resources. For instance, there are three `Detector` stages allowing at most three frames to be processed in parallel. The `FrameBalancer` stage is responsible for distributing the emitted frames from the `VideoSource` stage to the `Detector` stages applying a balancing strategy that pushes the next frame to a not busy stage. In order to run these stages in parallel, they have to be marked with `.async` making them execute in different contexts. The `DetectionMerge` stage fans in the stream by randomly taking one set of detected vehicle positions. The order in which these sets eventually arrive at the `Sink` stage does not matter because they are only persisted. The separate tracking process that is omitted in this design, takes care of rearranging them into their original sequence in order to track individual vehicles successfully throughout the video.

Maintaining the order of frames is not important when detecting vehicles. Therefore, the primary benefit of using a stream is to structure the process in a straightforward way. In comparison to the frame-pulling pattern that is based on several actors sending messages back and forth to signal demand for work, this design only requires some built-in stages that leverage powerful features of the Akka toolkit. The model is easily to understand what makes it favourable for future extension by simply adding more stages or connecting it with other graphs.